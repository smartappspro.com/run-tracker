# Run Tracker for Fitness Activity Tracking

Run Tracker is a fitness app to tack your daily activity i.e. Running, Jogging, Walking and Jumping. This app track your distance, duration, pace, speed, calories and more. This app uses GPS device to provide accurate distance ,speed and pace. It calculate your calorie according to your activity so it provides more accurate calories burn.

App has dual unit feature so you can use app in Km or Mile unit, as on your preference. This takes your basic body measurement i.e. weight, height, gender and age to calculate calories according your body.

App has very easy to use and clean user interface so any age user can use it. This app has calories graph to to show daily calorie burn.
App has following main features

-- Offline Working
-- View track and distance on Map
-- Milestone feature
-- Running Route
-- Average current pace and speed
-- GPS based positioning
-- Audio Coaching
-- Audio Cue for Duration (Adjustable)
-- Audio Cue for Distance (Adjustable)
-- Activities History
-- Live Activity Switch (Between Running, Jogging, Walking, Jumping)
-- Total calories, distance, duration view
-- Access music player during activity
-- Run app in background mode
-- Share app

Download from Playstore

https://play.google.com/store/apps/details?id=com.smartappspro.runtracker

